/***************************************************************************
 *                                                                         *
 *                           Proposal.scala                                *
 *                            -------------------                          *
 *   date                 : 9.30.2015                                      *
 *   email                : heath.french@utah.edu                          *
 *                                                                         *
 *                                                                         *
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   A copy of the license can be found in the license.txt file supplied   *
 *   with this software or at: http://www.gnu.org/copyleft/gpl.html        *
 *                                                                         *
 ***************************************************************************/

@SerialVersionUID(113L)
class Proposal(private val epoch : Long, private val transaction : Transaction) extends Ordered[Proposal] with Serializable{
  
  // grants access to the state and zxid values within the current transaction
  def getEpoch() : Long = epoch
  def getTransaction() : Transaction = transaction
  
  override def toString() : String = {
    val result : String = "<<" + epoch + ">, " + transaction.toString() + ">"
    return result
  }
  
  override def hashCode() : Int = {
    var epochHashCode : Int = 0
    var transactionHashCode : Int = 0
    if(epoch != null || transaction != null){
      epochHashCode = epoch.hashCode()
      transactionHashCode = transaction.hashCode()
    }
    return 17 * epochHashCode + transactionHashCode
  }
  
  override def equals(that : Any) : Boolean = {
    that match {
      case x : Proposal => {
        return (this.hashCode() == that.hashCode())
      }
      case _ => {
        return false
      }
    }
  }
  
  // overrides Ordered's implementation for compare
  override def compare(other : Proposal) : Int = {
    transaction.compare(other.getTransaction())
  }
}