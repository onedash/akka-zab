Akka ZAB is released under GNU General Public License. A copy of it is
contained in the archives in a file named license.txt.
A copy of the license can be found at http://www.gnu.org/copyleft/gpl.html.


Requirements: 

- Java 1.5 (javac compiler specifically)

- Scala 2.11.7

- SBT to compile Akka ZAB.


Available files: 

Akka-ZAB: Contains the complete Open Chord version 1.x.x with src,
documentation, and compiled files.


Installation:

Just unzip the desired zip file to any directory.
From here, get into the Akka-ZAB folder using a terminal.
To run the program, simply type in "sbt run".
If you wish to recompile the code at any point, just type in "sbt clean"
and then run it again.
